#**Docker installion**

#Update existing list of packages

sudo apt-get update

#Install a few prerequisite packages which let apt use packages over HTTPS

sudo apt install apt-transport-https ca-certificates curl software-properties-common

#Add the GPG key for the official Docker repository to the system:

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

#Add the Docker repository to APT sources

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

#Update the package database with the Docker packages from the newly added repo

sudo apt update

#Install Docker

sudo apt install docker-ce

#Check that it’s running

sudo systemctl status docker
