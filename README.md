# **Tikal kubernetes_redis**

# Deploy kuberbets redis with security enabled and configmap

## _Prerequisites:_

- install docker 

- install minikube

- install kubectl

------

mkdir redis

cd redis

git clone https://gitlab.com/bmd738/tikal_redis.git

.

### _Create minikube cluster_

minikube start --vm-driver=virtualbox 

.

## _Create pod - redis_

_Befor we create the pod we hava to set up the secret.yml_

kubectl apply -f redis-secret.yaml

### _Create pod and internal service_

kubectl apply -f redis.yaml

.

## _Create pod - redisinsight_

_This pos is contain the web application_


## _Create configmap_

_First we have to configre where is the database store_

kubectl apply -f configmap.yaml


### _Create pod and external service_

kubectl apply -f redisinsight.yaml

.

## _Run the application_

minikube service redisinsight










